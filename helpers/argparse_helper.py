import argparse


def arguments_parser_setup():
    """
    :return: void(?)
    """
    """
        Unit test(s):
        - Ensure mandatory args are parsed
            - fails appropriately
        - Defaults are set
            - g to None
            - N to 5
            - o to out.pdf
    """

    parser = argparse.ArgumentParser(description='Graph some data from segments from Strava')

    parser.add_argument('lat1', metavar='<lat1>', type=float)
    parser.add_argument('long1', metavar='<long1>', type=float)
    parser.add_argument('lat2', metavar='<lat2>', type=float)
    parser.add_argument('long2', metavar='<long2>', type=float)
    parser.add_argument('accessKey', type=str)

    parser.add_argument('-g', metavar='\"M\" \"F\"', type=str, default=None, help='filter the time segments by gender')
    parser.add_argument('-n', metavar='N', type=int, default=5,
                        help='number of time segments to display (defaults to display up to 5)')
    parser.add_argument('-o', metavar='filename', type=str, default='out.pdf',
                        help='output filename to save to (defaults to out.pdf)')

    return parser
