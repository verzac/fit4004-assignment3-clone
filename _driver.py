"""
THIS FILE IS ONLY USED FOR EXPERIMENTING, TELL @verzac TO DELETE THIS IF IT PASSES THROUGH TO THE PULL REQUEST
"""
from typing import List, Tuple

import services.strava_api_service as gs
import json
import app

from dtos import leaderboard_dto, explore_segment_dto

API_KEY = "2e48a2e495ea88df57a5da0b0b0a988d1f6a8fef"


def get_segment():
    result = gs.call_get_for_segments(lat2=-37.5112737,
                                      long2=145.5125288,
                                      lat1=-38.4338593,
                                      long1=144.5937418,
                                      api_key=API_KEY)
    print(json.dumps(result, indent=4, sort_keys=True))
    lo = explore_segment_dto.build_list_of_segments(result)
    print(len(lo))
    return lo


def get_leaderboard():
    result = gs.call_get_for_leaderboard(segment_id=816944, api_key=API_KEY)
    print(json.dumps(result, indent=4, sort_keys=True))
    lo = leaderboard_dto.build_strava_leaderboard(result)
    print([json.dumps(o.__dict__) for o in lo.entries])


def get_the_whole_thing():
    n = 5
    result = gs.call_get_for_segments(lat2=-37.5112737,
                                      long2=145.5125288,
                                      lat1=-38.4338593,
                                      long1=144.5937418,
                                      api_key=API_KEY)
    result.sort(key=lambda segment: segment.distance, reverse=True)
    segment_list = result[:n]  # type: List[explore_segment_dto.Segment]
    print(segment_list)
    segment_leaderboard_list = list()# type: List[Tuple[explore_segment_dto.Segment, leaderboard_dto.StravaLeaderBoard]]
    for segment in segment_list:
        print(segment)
        segment_leaderboard_list.append(
            (segment, gs.call_get_for_leaderboard(segment_id=segment.segment_id, api_key=API_KEY)))
    for segment, leaderboard in segment_leaderboard_list:
        print("---")
        print(segment)
        print(leaderboard.entries[0].__dict__)


if __name__ == "__main__":
    segment_list = get_the_whole_thing()
