class StravaLeaderBoard:
    def __init__(self, entries):
        """
        :param list[StravaLeaderBoardEntry] entries:
        """
        self.entries = entries

    def get_fastest_time(self):
        # It is assumed that API returns an ordered leaderboard.
        assert self.entries[0].rank == 1, "First entry in the entry object object is not the fastest (Not rank 1)"
        return self.entries[0].elapsed_time


class StravaLeaderBoardEntry:
    def __init__(self, athlete_name, elapsed_time, moving_time, start_date, start_date_local, rank):
        """
        :param str athlete_name:
        :param int elapsed_time:
        :param int moving_time:
        :param str start_date:
        :param str start_date_local:
        :param int rank:
        """
        self.athlete_name = athlete_name
        self.elapsed_time = elapsed_time
        self.moving_time = moving_time
        self.start_date = start_date
        self.start_date_local = start_date_local
        self.rank = rank


def build_strava_leaderboard(json_dict):
    entries_dict_list = json_dict["entries"]
    return StravaLeaderBoard(entries=[build_strava_leaderboard_entry(dct) for dct in entries_dict_list])


def build_strava_leaderboard_entry(dct):
    """
    :param dict[str, Any] dct:
    :rtype: StravaLeaderBoardEntry
    """
    return StravaLeaderBoardEntry(athlete_name=dct["athlete_name"],
                                  elapsed_time=dct["elapsed_time"],
                                  moving_time=dct["moving_time"],
                                  start_date=dct["start_date"],
                                  start_date_local=dct["start_date_local"],
                                  rank=dct["rank"])
