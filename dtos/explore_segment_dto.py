class Segment:
    def __init__(self, segment_id, name, distance):
        """
        :param int segment_id:
        :param str name:
        :param int distance:
        """
        self.segment_id = segment_id
        self.name = name
        self.distance = distance

    def __str__(self):
        return str(self.segment_id) + ":" + str(self.distance)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        assert isinstance(other, Segment)
        return self.segment_id == other.segment_id and self.name == other.name and self.distance == other.distance


def build_list_of_segments(dct):
    """
    :param dict[str, Any] dct:
    :return:
    :rtype: list[Segment]
    """
    segment_dict_list = dct["segments"]
    return [build_segment(entry_dct) for entry_dct in segment_dict_list]


def build_segment(dct):
    """
    :param dict[str, Any] dct:
    :return: a Segment object
    :rtype: Segment
    """
    return Segment(segment_id=dct["id"],
                   name=dct["name"],
                   distance=dct["distance"])
