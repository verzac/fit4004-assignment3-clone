# Test Strategy
The test cases for the unit test conducted for this program are defined in Python files under /test. 

Upon a commit, the continuous integration environment runs the tests scripts in the test folder:
```python -m unittest discover -s test```

## Testing Strategy.
Overall the team mainly focused on code coverage and boundary tests to ensure the correct state of the program at every level. The program was developed using smaller modules each of which were unit tested thoroughly to ensure functional correctness.

For each module, the team focused on code coverage and assertion testing to verify the code’s behaviour. The team is using series of valid and invalid arguments to test the module. Assertion tests use valid inputs in order to verify that the program will always output the correct results given correct parameters (i.e. through assertion testing in order to ensure the integrity of the data). For example, assertion testing is used to verify the integrity of the data built and outputted in the API calls.

The boundary cases are used to test the behaviour of the program under exceptional cases. For example, the boundaries for latitudes and longitudes are tested when testing `strava_api_service.call_get_for_segments()`.

The team is also using error guessing approach to simulate situations that are most likely to result in the incorrect behaviour. This is different from providing incorrect parameters as the parameters provided in this approach are valid but do not result in correct program behavior.

## Code Coverage
The team is using coverage.py to help develop a test suite that will yield maximum code coverage. Code coverage is used to measure the effectiveness of the suite and the team has aimed for maximum coverage in most efficient way possible. Each test run results a coverage report and team uses them to reflect back on the quality and quantity of tests to ensure maximum coverage. We aim to get more than 90% code coverage on our code (except test cases and/or files that are not related to the app’s main functionality such as _driver.py).

See [here](https://nikhilsharma66.gitlab.io/FIT4004-Assignment3/index.html) for an up-to-date code coverage report. **Note that the report is generated automatically after each commit to the `master` branch**