import matplotlib
import matplotlib.pyplot as plt


def graph_data_to_file(plot_data, filename="out.pdf"):
    """
    :param filename:
    :param plot_data: List of plot data [x_axis_values, y_axis_values, x_tick_labels]
    :return: visual pyplot graph
    """

    fig = plt.figure()
    plt.xticks(plot_data[0], plot_data[2], rotation=45, ha="right")
    plt.yticks()
    plt.ylabel("Time (s)")
    plt.bar(plot_data[0], plot_data[1])

    fig.savefig(filename, bbox_inches='tight')


def setup_graph_data(graph_data_tuple):
    """
        :param graph_data_tuple: array of tuples, each tuple containing (segment_dto, leaderboard_dto) objects
        :return: x_axis_values array of range values from 1 to number of segments to graph (e.g. [1, 2, 3, ...])
        :return: y_axis_values array of fastest times corresponding to lengthiest (by distance) segments
        :return: x_tick_labels array of names from each of the segments
    """

    x_axis_values = []
    y_axis_values = []
    x_tick_labels = []

    # [0] corresponds to segment_dto
    # [1] corresponds to leaderboard
    for i in graph_data_tuple:
        y_axis_values.append(i[1].get_fastest_time())
        x_tick_labels.append(i[0].name)
        x_axis_values.append(len(x_tick_labels))

    return x_axis_values, y_axis_values, x_tick_labels
