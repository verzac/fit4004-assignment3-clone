"""
Author:             Zi C
Date Created:       11-May-18
Description:        Contains the main file, and accepts arguments when run on the commandline.
"""

import json  # for testing a prettyprint
import requests
from dtos import leaderboard_dto, explore_segment_dto

ROOT_URL = "https://www.strava.com/api/v3"


def validate_latitude_longitudes(lat1, long1, lat2, long2):
    assert type(lat1) is float
    assert type(long1) is float
    assert type(lat2) is float
    assert type(long2) is float

    assert -90 <= lat1 <= 90, "latitude 1 must be between -90 to 90"
    assert -90 <= lat2 <= 90, "latitude 2 must be between -90 to 90"
    assert -180 <= long1 <= 180, "longitude 1 must be between -180 to 180"
    assert -180 <= long2 <= 180, "longitude 2 must be between -180 to 180"


def call_get_for_segments(lat1, long1, lat2, long2, api_key):
    """
    :param float lat1: latitude value, must be between -90 and 90
    :param float long1: longitude value, must be between -180 and 180
    :param float lat2: latitude value, must be between -90 and 90
    :param float long2: longitude value, must be between -180 and 180
    :param str api_key:
    :return: array of Segment objects
    :rtype: list[explore_segment_dto.Segment]
    """
    validate_latitude_longitudes(lat1, long1, lat2, long2)

    url = ROOT_URL + "/segments/explore"

    bounds_string = "{}, {}, {}, {}".format(lat1, long1, lat2, long2)

    querystring = {"bounds": bounds_string}

    headers = {
        'Authorization': "Bearer " + api_key,
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers, params=querystring)

    # below is just json formatting with pretty print
    reply = response.text
    parsed = json.loads(reply)
    return explore_segment_dto.build_list_of_segments(parsed)


def call_get_for_leaderboard(segment_id, api_key, gender=None, number_of_ranks_returned=1):
    """
    :param int segment_id: ID of segment to get leaderboard from
    :param str or None gender: filter by gender; must either be "M" or "F"
    :param str api_key: the API key used to call the API
    :param int number_of_ranks_returned: defines number of items returned in a call
    :return: dict file containing the response
    :rtype: leaderboard_dto.StravaLeaderBoard
    """
    if gender is not None:
        valid_gender = ["M", "F"]
        assert gender in valid_gender, "Gender must be one of the following: {}.".format(valid_gender)

    url = ROOT_URL + "/segments/{}/leaderboard".format(str(segment_id))
    querystring = {
        "gender": gender,
        "per_page": number_of_ranks_returned
    }
    headers = {
        'Authorization': "Bearer " + api_key,
        'Cache-Control': "no-cache"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    parsed = json.loads(response.text)
    return leaderboard_dto.build_strava_leaderboard(parsed)

