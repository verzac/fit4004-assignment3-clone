from dtos import leaderboard_dto, explore_segment_dto
from helpers import argparse_helper
from services import strava_api_service, graph_service
from typing import List, Tuple


def get_quickest_times_from_longest_n_segments(lat1, long1, lat2, long2, access_key, g, n):
    """
    :return: A tuple array (segment_dto, leaderboard_dto) of the n-lengthiest segments, with the fastest times of each
    """
    # Get Segments
    result = strava_api_service.call_get_for_segments(lat1, long1, lat2, long2, access_key)

    # Sort by distance, from longest dist. to shortest
    result.sort(key=lambda segment: segment.distance, reverse=True)

    # Create segment and leaderboard lists
    segment_list = result[:n]

    graph_data = list()  # type: List[Tuple[explore_segment_dto.Segment, leaderboard_dto.StravaLeaderBoard]]

    # Get leaderboards from each of the segments, filtered by gender if applicable
    for segment in segment_list:
        graph_data.append(
            (segment, strava_api_service.call_get_for_leaderboard(segment_id=segment.segment_id, api_key=access_key, gender=g)))

    return graph_data


def main():
    # parse arguments
    parser_main = argparse_helper.arguments_parser_setup()
    cmd_args = parser_main.parse_args()

    # get and sort data from API
    graph_data = get_quickest_times_from_longest_n_segments(cmd_args.lat1, cmd_args.long1, cmd_args.lat2, cmd_args.long2
                                                            , cmd_args.accessKey, cmd_args.g, cmd_args.n)

    # graph and output data to file
    graph_labels = graph_service.setup_graph_data(graph_data)
    graph_service.graph_data_to_file(graph_labels, cmd_args.o)

if __name__ == "__main__":
    main()
