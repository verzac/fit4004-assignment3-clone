from dtos import leaderboard_dto, explore_segment_dto
import services.graph_service
import unittest
import os


class TestGraphService(unittest.TestCase):
    """
        Unit test(s) (should be testing the arrays just prior to graphing):
        - Segmented correctly (to values of n, and if there are only segments less than n)
        - Values set correctly (x_axis_values, y_axis_values, my_xticks
    """

    def test_setup_graph_data_should_create_graph_labels_from_tuple_data(self):
        test_tuple = self.setup_tuple_data()

        self.assertEqual(services.graph_service.setup_graph_data(test_tuple),
                         ([1, 2], [400, 500], ["ID 1, Dist 500 Segment", "ID 2, Dist 600 Segment"]))

    def test_setup_graph_data_should_retain_output_order_from_tuple_data(self):
        test_tuple = self.setup_tuple_data()

        for i in range(len(services.graph_service.setup_graph_data(test_tuple)[0])):
            self.assertEqual(services.graph_service.setup_graph_data(test_tuple)[1][i], test_tuple[i][1].get_fastest_time())
            self.assertEqual(services.graph_service.setup_graph_data(test_tuple)[2][i], test_tuple[i][0].name)

    def test_graph_data_to_file_should_create_file(self):
        test_tuple = self.setup_tuple_data()
        test_graph_data = services.graph_service.setup_graph_data(test_tuple)

        services.graph_service.graph_data_to_file(test_graph_data)

        try:
            f = open('out.pdf', 'r')
            f.close()
        except FileNotFoundError:
            self.fail('File did not create correctly.')

    def test_graph_data_to_file_should_create_file_with_naming(self):
        test_tuple = self.setup_tuple_data()
        test_graph_data = services.graph_service.setup_graph_data(test_tuple)

        services.graph_service.graph_data_to_file(test_graph_data, 'aNewFile.pdf')

        try:
            f = open('aNewFile.pdf', 'r')
            f.close()
        except FileNotFoundError:
            self.fail('File did not create with new name correctly.')

    def setup_tuple_data(self):
        test_entry_1 = leaderboard_dto.StravaLeaderBoardEntry("Test Athlete 1", 400, 400, "24/05/18", "24/05/18", 1)
        test_entry_2 = leaderboard_dto.StravaLeaderBoardEntry("Test Athlete 2", 410, 410, "24/05/18", "24/05/18", 2)
        test_segment_1 = explore_segment_dto.Segment(1, "ID 1, Dist 500 Segment", 500)
        test_leaderboard_1 = leaderboard_dto.StravaLeaderBoard([test_entry_1, test_entry_2])

        test_entry_3 = leaderboard_dto.StravaLeaderBoardEntry("Test Athlete 3", 500, 500, "24/05/18", "24/05/18", 1)
        test_entry_4 = leaderboard_dto.StravaLeaderBoardEntry("Test Athlete 4", 510, 510, "24/05/18", "24/05/18", 2)
        test_segment_2 = explore_segment_dto.Segment(2, "ID 2, Dist 600 Segment", 600)
        test_leaderboard_2 = leaderboard_dto.StravaLeaderBoard([test_entry_3, test_entry_4])

        tuple_data = [(test_segment_1, test_leaderboard_1), (test_segment_2, test_leaderboard_2)]

        return tuple_data
