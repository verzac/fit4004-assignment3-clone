from helpers.argparse_helper import *
import unittest


class TestArgumentsParser(unittest.TestCase):
    def test_arguments_parser_mandatory_args(self):
        test_parser = arguments_parser_setup()

        with self.assertRaises(BaseException):
            test_parser.parse_args([""])

        with self.assertRaises(BaseException):
            test_parser.parse_args(["1", "3", "4", "APIKey"])

        with self.assertRaises(BaseException):
            test_parser.parse_args(["1", "2", "3", "4"])

        with self.assertRaises(BaseException):
            test_parser.parse_args(["APIKey"])

    def test_arguments_parser_defaulting(self):
        test_parser = arguments_parser_setup()
        test_parser_namespace = test_parser.parse_args(["1", "2", "3", "4", "APIKey"])

        self.assertEqual(test_parser_namespace.g, None)
        self.assertEqual(test_parser_namespace.n, 5)
        self.assertEqual(test_parser_namespace.o, "out.pdf")
