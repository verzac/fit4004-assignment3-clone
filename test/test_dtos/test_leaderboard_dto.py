from dtos import leaderboard_dto
import unittest


class TestLeaderboardDto(unittest.TestCase):
    def test_build_leaderboard(self):
        leaderboard_list = leaderboard_dto.build_strava_leaderboard(TEST_DATA)
        list_of_leaderboard_dct = TEST_DATA["entries"]
        for leaderboard_dct in list_of_leaderboard_dct:
            assert isinstance(leaderboard_dct, dict)
            is_found = False
            for leaderboard in leaderboard_list.entries:
                if leaderboard_dct["athlete_name"] == leaderboard.athlete_name and \
                   leaderboard_dct["elapsed_time"] == leaderboard.elapsed_time and \
                   leaderboard_dct["moving_time"] == leaderboard.moving_time and \
                   leaderboard_dct["start_date"] == leaderboard.start_date and \
                   leaderboard_dct["start_date_local"] == leaderboard.start_date_local and \
                   leaderboard_dct["rank"] == leaderboard.rank:
                    is_found = True
                    break
            self.assertTrue(is_found)

    def test_get_fastest_time_should_return_fastest_time(self):
        leaderboard_list = leaderboard_dto.build_strava_leaderboard(TEST_DATA)

        self.assertEqual(leaderboard_list.get_fastest_time(), TEST_DATA['entries'][0]['elapsed_time'])

    def test_get_fastest_time_should_fail_on_wrong_rank_entry(self):
        # case if the first entry is not of rank 1
        leaderboard_list = leaderboard_dto.build_strava_leaderboard(TEST_DATA_INVALID_CASE)

        with self.assertRaises(AssertionError):
            self.assertRaises(leaderboard_list.get_fastest_time())

TEST_DATA = \
{
    "effort_count": 18743,
    "entry_count": 18743,
    "kom_type": "kom",
    "entries": [
        {
            "athlete_name": "Jimmy W.",
            "elapsed_time": 524,
            "moving_time": 524,
            "start_date": "2017-12-23T04:23:19Z",
            "start_date_local": "2017-12-23T15:23:19Z",
            "rank": 1
        },
        {
            "athlete_name": "Shane M.",
            "elapsed_time": 533,
            "moving_time": 533,
            "start_date": "2012-09-27T01:16:14Z",
            "start_date_local": "2012-09-27T11:16:14Z",
            "rank": 2
        },
        {
            "athlete_name": "Cyrus M.",
            "elapsed_time": 535,
            "moving_time": 535,
            "start_date": "2016-04-27T06:20:30Z",
            "start_date_local": "2016-04-27T16:20:30Z",
            "rank": 3
        },
        {
            "athlete_name": "Stephen L.",
            "elapsed_time": 537,
            "moving_time": 537,
            "start_date": "2016-04-27T06:20:33Z",
            "start_date_local": "2016-04-27T16:20:33Z",
            "rank": 4
        },
        {
            "athlete_name": "Andrew S.",
            "elapsed_time": 545,
            "moving_time": 545,
            "start_date": "2016-12-12T19:44:53Z",
            "start_date_local": "2016-12-13T06:44:53Z",
            "rank": 5
        },
        {
            "athlete_name": "Aidan F.",
            "elapsed_time": 546,
            "moving_time": 546,
            "start_date": "2016-12-12T19:44:50Z",
            "start_date_local": "2016-12-13T06:44:50Z",
            "rank": 6
        },
        {
            "athlete_name": "Matt C.",
            "elapsed_time": 548,
            "moving_time": 548,
            "start_date": "2016-12-12T19:44:49Z",
            "start_date_local": "2016-12-13T06:44:49Z",
            "rank": 7
        },
        {
            "athlete_name": "Damien B.",
            "elapsed_time": 548,
            "moving_time": 548,
            "start_date": "2016-12-12T19:44:52Z",
            "start_date_local": "2016-12-13T06:44:52Z",
            "rank": 7
        },
        {
            "athlete_name": "Aaron P.",
            "elapsed_time": 552,
            "moving_time": 552,
            "start_date": "2016-12-12T19:44:48Z",
            "start_date_local": "2016-12-13T06:44:48Z",
            "rank": 9
        },
        {
            "athlete_name": "Lachlan V.",
            "elapsed_time": 553,
            "moving_time": 553,
            "start_date": "2016-12-12T19:44:49Z",
            "start_date_local": "2016-12-13T06:44:49Z",
            "rank": 10
        }
    ]
}

TEST_DATA_INVALID_CASE = \
{
    "effort_count": 18743,
    "entry_count": 18743,
    "kom_type": "kom",
    "entries": [
        {
            "athlete_name": "Jimmy W.",
            "elapsed_time": 524,
            "moving_time": 524,
            "start_date": "2017-12-23T04:23:19Z",
            "start_date_local": "2017-12-23T15:23:19Z",
            "rank": 2
        },
        {
            "athlete_name": "Shane M.",
            "elapsed_time": 533,
            "moving_time": 533,
            "start_date": "2012-09-27T01:16:14Z",
            "start_date_local": "2012-09-27T11:16:14Z",
            "rank": 1
        },
        {
            "athlete_name": "Cyrus M.",
            "elapsed_time": 535,
            "moving_time": 535,
            "start_date": "2016-04-27T06:20:30Z",
            "start_date_local": "2016-04-27T16:20:30Z",
            "rank": 5
        },
        {
            "athlete_name": "Stephen L.",
            "elapsed_time": 537,
            "moving_time": 537,
            "start_date": "2016-04-27T06:20:33Z",
            "start_date_local": "2016-04-27T16:20:33Z",
            "rank": 4
        },
        {
            "athlete_name": "Andrew S.",
            "elapsed_time": 545,
            "moving_time": 545,
            "start_date": "2016-12-12T19:44:53Z",
            "start_date_local": "2016-12-13T06:44:53Z",
            "rank": 3
        },
        {
            "athlete_name": "Aidan F.",
            "elapsed_time": 546,
            "moving_time": 546,
            "start_date": "2016-12-12T19:44:50Z",
            "start_date_local": "2016-12-13T06:44:50Z",
            "rank": 6
        },
        {
            "athlete_name": "Matt C.",
            "elapsed_time": 548,
            "moving_time": 548,
            "start_date": "2016-12-12T19:44:49Z",
            "start_date_local": "2016-12-13T06:44:49Z",
            "rank": 7
        },
        {
            "athlete_name": "Damien B.",
            "elapsed_time": 548,
            "moving_time": 548,
            "start_date": "2016-12-12T19:44:52Z",
            "start_date_local": "2016-12-13T06:44:52Z",
            "rank": 7
        },
        {
            "athlete_name": "Aaron P.",
            "elapsed_time": 552,
            "moving_time": 552,
            "start_date": "2016-12-12T19:44:48Z",
            "start_date_local": "2016-12-13T06:44:48Z",
            "rank": 9
        },
        {
            "athlete_name": "Lachlan V.",
            "elapsed_time": 553,
            "moving_time": 553,
            "start_date": "2016-12-12T19:44:49Z",
            "start_date_local": "2016-12-13T06:44:49Z",
            "rank": 10
        }
    ]
}