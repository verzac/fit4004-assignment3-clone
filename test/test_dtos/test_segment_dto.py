import unittest
from dtos import explore_segment_dto


class TestSegmentDto(unittest.TestCase):
    def test_segment_dto_builder(self):
        segment_list = explore_segment_dto.build_list_of_segments(TEST_DATA)
        list_of_segment = TEST_DATA["segments"]
        for segment_dict in list_of_segment:
            self.assertTrue(explore_segment_dto.Segment(segment_id=segment_dict["id"],
                                                        name=segment_dict["name"],
                                                        distance=segment_dict["distance"]) in segment_list)


TEST_DATA = \
{
    "segments": [
        {
            "avg_grade": 4.1,
            "climb_category": 2,
            "climb_category_desc": "3",
            "distance": 6695.5,
            "elev_difference": 277.2,
            "end_latlng": [
                -37.863013,
                145.351357
            ],
            "id": 374953,
            "name": "1/20",
            "points": "`z`fFs`}uZpDcB\\Kf@Kx@KR?hCHhAMh@g@Lq@?cCv@aE`@yApAiDh@_BXuAVo@v@g@`AIt@B|@VjAr@p@t@fB`Cl@|@`@bAj@h@xAp@rAx@n@Xh@Xp@HXQLi@BwCJiB@uAAq@FwAPoARcAPk@Zs@n@w@bAy@`Ao@j@[LMJq@mAsEGa@cA_JCwBBsADk@b@uCZoAf@q@TOp@Ob@Eh@ObAk@lAi@rCg@dAWj@Ih@QRUFO^sBI}@EKo@i@IKGs@Bw@Hg@\\s@^o@Pi@JaBO{@]Ou@G]Fe@P}@FUDg@h@Kd@OrBWlA_@|@UXa@\\}@j@g@NkACcBc@eAMw@?eAFuBXMB}B~@}@@a@e@Kc@i@qCc@uAIiAT}AN]xAoCv@qBLo@HeA@sAIiAOaAi@iBc@QqAP_@GmA[yAg@YEeDYmDm@GA]YWk@LaAT_@`@c@lAw@jB{Ah@gABI@]WcAAgAF{@Ls@Ri@PQlB_Ad@OfB[bBo@v@Md@MvAYpA?dACXELGO]Ik@M_@}@s@{Ao@kB}@WOaA}@QYSm@Io@?WEWO_@_@s@SYWm@CWHi@p@qAd@mAHm@?{@WqCa@_DSoAUeAW{@g@}@",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.856482,
                145.316107
            ]
        },
        {
            "avg_grade": 0.2,
            "climb_category": 0,
            "climb_category_desc": "NC",
            "distance": 6613.6,
            "elev_difference": 31.8,
            "end_latlng": [
                -37.8065209463239,
                145.01060301438
            ],
            "id": 612405,
            "name": "Yarra Blvd, Kew (southbound)",
            "points": "doseFagdtZGv@Uf@]d@Sj@CLKx@Eh@GzA@bA@|@FtAVzDNpAFbCP|Aj@tC@jBDd@DHR\\FD^LVAVGTQ`ByCn@{@xBmC\\Yn@[xDeB\\O^I\\CN@\\Hr@\\rCjB`@`@Rd@FXJfA@\\Ej@a@nCK|@Cj@@tBGhAKl@cA`DKp@AXDVHVl@XTBTARGPQPIH{@D_EJy@dAmD~AuCTWr@c@dA_@PMV]JW|@uERo@Ns@j@gApAcB`@SXGZ?~@Jd@ChAYXEf@Ch@Ff@Rd@ZZXJL\\p@b@dARZXR\\HNA`@I^OXYP]VwAPq@HMVYZO^EP@dBRl@@j@Mj@Qh@KZDf@TVTvAtCl@xANl@ZjBDf@BrACj@I|@i@lCG~BKv@Mb@OfAe@v@a@rAgArBm@~@Q\\IZG~AKn@QZWT[N_@Hq@Bi@NSRORQ`@i@`BEd@@XDXPd@lCxBV^JRJd@L|BZt@d@bAPVTV\\NTBVFb@`@RHf@HbCFb@H^TZ`@Vl@d@~ANd@~AvCTh@Pl@LlARpG?lAIn@g@~BWr@e@`AYx@GRCl@Dj@DTRh@Vf@\\`@^P^D^I\\SV]J_@Dc@?c@M_ECuJF}AZ_CFq@KcE@q@EqAU_DOgACo@@_APyBVmAL[",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.7881857007742,
                145.025291796774
            ]
        },
        {
            "avg_grade": 4.8,
            "climb_category": 3,
            "climb_category_desc": "2",
            "distance": 7286.4,
            "elev_difference": 350.6,
            "end_latlng": [
                -37.535922,
                145.340194
            ],
            "id": 376351,
            "name": "Kinglake",
            "points": "`tidF}_zuZaDsB_Bm@kDg@}@]q@[cAm@y@s@gAiAaAkAi@cAwA{Dq@}@[AS^i@n@e@HGAc@Uy@kA_@y@y@mCYqAUgB[cAg@{@{@cAyB{C_@{@_@k@kA{@s@aAw@yAqAqCOcASq@{A}CEEa@KoAC_@W[m@Gy@PiAL}ACuAYyAe@oA_@e@u@Sw@]Q]?o@R{@tAkB^w@DuBNeA?}@SaD[iAa@u@e@a@c@?gA\\s@CSY?k@TaAF{@Mu@e@s@a@e@qBQ[_@Ec@Jk@^kAD{@S[_A}@k@m@Oq@Bm@Zk@p@g@|@[p@]Dc@AIe@qAWaAAg@Lk@j@]`BIx@Q`@_@?]SmAA_AOo@s@cAKo@TyB?IWmAOc@k@]a@m@YwAc@uAa@_AIM_@@u@\\iBb@[Pw@r@WLg@AWKYYe@oAUgBMqABg@XkAD]E_BIsAIg@[_@a@AYEe@c@WkAM{AU{@K[s@cA[_AWgAe@YQUm@mA}A{Aq@_@gAa@iA[o@C_BZy@Bg@GkAYw@McCKkB?mJt@u@JeCl@{@JcELcB?_DI_De@{@EuDKs@Em@Mo@[_@[s@}@i@_@cBSu@Sq@c@o@Mu@A_AFe@RK\\Ch@Mr@YXm@Py@Cu@Q_@a@cAyBi@s@c@MaA?q@S_DaCk@SgDO",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.573927,
                145.300638
            ]
        },
        {
            "avg_grade": 7.8,
            "climb_category": 2,
            "climb_category_desc": "3",
            "distance": 3009.2,
            "elev_difference": 236.0,
            "end_latlng": [
                -38.353653112426,
                144.950448274612
            ],
            "id": 632890,
            "name": "Wonga Gate to Summit (Bunjil)",
            "points": "`_`iFoavsZnBXn@@~@Ll@DHBLJF@r@FP?ZFHJ@JCXeAzAK^APDNTZJFL?JET_@d@i@JGPE`AEpBNZAd@Ip@QJAPDNN@HGLm@~@_A`AMZCL?NDNFJHDP?REd@[nB_A\\DFFDHCTITi@~@Kb@@VJJH@NCJKfAuAPMXO|@YRC^CR@b@Lb@b@J`@?PGXu@jBG^ARBNn@rCNj@LRVXt@f@PFF@BI?EOQg@e@KWKs@BkAFe@L]x@sAn@oAZsAFc@CgCDMFEJ?JBFFBHRvECf@Ir@e@zAANFTTHJAvAi@ZCZ?d@Lj@X|@ZtAPl@NVTRX`ApCXp@V\\NHRBXEnAi@PMPWN[He@?_AAQM_@_@w@s@{@QW{A}CUUoAs@",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -38.343681162223,
                144.952729158103
            ]
        },
        {
            "avg_grade": 4.9,
            "climb_category": 2,
            "climb_category_desc": "3",
            "distance": 6184.8,
            "elev_difference": 310.2,
            "end_latlng": [
                -37.8556508850306,
                145.366091681644
            ],
            "id": 622862,
            "name": "The Wall Complete",
            "points": "tcdfF}smvZLTPdAz@rDX~AHr@F~BDx@N|AJb@Zj@TR^j@f@d@x@j@d@VZV`@RV\\PNPHr@t@f@\\`@h@TRp@dA^h@v@zAn@|Bh@nATz@PdABl@ITG@Qa@Eg@Qm@m@cAa@e@Mc@i@u@aA{@oAw@e@Uu@M{AEcAHc@J_A`@cB~@_BdA_Cz@OB[@}@GqCc@aBQ]ISCg@D[Pi@f@m@pAC`@SdAKv@AVDrBX~ADn@An@Dx@El@@XCXB~@AJIZEf@Ap@@P\\dBDLz@~Av@`A`@t@Jn@Np@TvBBz@Jb@F|@Tn@Vb@n@tAXd@\\Hb@Tj@`@l@n@\\p@BLF`AJb@B|@Fn@@z@QdA_@dAUh@ORSNeBl@k@VoCz@WLuAfB]n@a@l@m@r@GPKNm@h@oAr@ONw@Xg@Lc@Ti@FSEs@BSAaAUgBk@wB[cABgANaAAS@qAp@kCbAw@f@[LYRKNADc@Nu@Ni@A_@@e@Fo@P[@OFBAq@DkC`@sAN[FWHwA\\]Nw@La@N_@Dm@Z_@J[Ni@Jw@XeAvAYLKL[j@e@r@IF]p@y@l@sAj@e@Jq@HgBU[?M@s@d@s@n@g@`@w@hAOXKZIx@JjDFbABXHbCJhAXfArAlBLV\\bAVjAVlB^tAZzC",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.8733851481229,
                145.401114709675
            ]
        },
        {
            "avg_grade": -0.1,
            "climb_category": 0,
            "climb_category_desc": "NC",
            "distance": 6688.5,
            "elev_difference": 33.6,
            "end_latlng": [
                -37.7881090901792,
                145.02538651228
            ],
            "id": 615026,
            "name": "Yarra Blvd, Kew (northbound)",
            "points": "lcweFokatZ_@DKNg@`CGv@Af@@vATlCTbDDtFCvA]hCEdA@lJBzALfCAf@Kf@U^OJ_@NQBe@?a@KMKY[S_@]}@Kk@Ck@@m@D[Pi@~@uBL[b@aCFq@AaB[uFM{@YaAsB{DSm@c@}AMUSSSMe@KWAoAD}@G_@Ia@QaAk@a@Q[c@S_A[s@GSKiAAs@EYSk@_@a@}@k@u@o@UYGMGQEe@@i@P_Ax@gBNSNQf@W|AS\\SJONg@Fk@BqALg@Rg@^a@h@{@jA_Ch@_B\\eBFy@DeBB[Hi@TeAHo@DuACw@WkBKe@Uw@yBsEOM]Wa@G_@BcA^k@Lo@@aBSM?[FUNQPORMd@[zAKZOVURYJg@FMAYGe@Y_@e@u@gBYc@]]UMWKWEo@?]DgB`@[BoAI_@DOFIJi@v@m@v@g@`AUv@aAfFMf@IPY\\OHgA^c@R]b@yAlC_@bAe@rBKl@En@?lCARKb@QZUP[FY@KCWIIGKGOUMi@Eo@Hm@fAgDNy@HkAEkB@m@l@aF@q@Iy@Og@Ye@c@]iDgBi@KUAS@i@JaGxCe@f@mCjDqAzBe@l@SJSFa@AWISUIOOgAAUDiAGq@]aBUeBK_Da@uEOwE@y@Du@TcCDYVq@n@iA@]EI",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.8067899215966,
                145.010640062392
            ]
        },
        {
            "avg_grade": 4.7,
            "climb_category": 1,
            "climb_category_desc": "4",
            "distance": 2023.7,
            "elev_difference": 95.8,
            "end_latlng": [
                -38.1948339007795,
                145.123834563419
            ],
            "id": 627537,
            "name": "Two Bays Climb (Cycle 2 Max)",
            "points": "llehFexttZ]]MSeBcEy@eA{@g@{As@g@c@[]w@mAe@{@k@wAi@aAOk@KSe@q@i@m@KYo@k@{@aBuC_Di@w@Sk@Ic@Qm@Ki@i@}A]_B]kAsAkDi@}Cg@gB]}@S_@U[m@e@i@S_@YUg@e@u@_Ao@IKy@{AgBuA]k@UW]WwAi@_@IoAQaAPo@D_@Ja@@o@LWJoDd@",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -38.2075894810259,
                145.109956311062
            ]
        },
        {
            "avg_grade": -0.1,
            "climb_category": 0,
            "climb_category_desc": "NC",
            "distance": 8455.3,
            "elev_difference": 21.5,
            "end_latlng": [
                -38.008874,
                145.085937
            ],
            "id": 619676,
            "name": "Black Rock to Mordialloc",
            "points": "voxfFqrbtZ`PeG~CeBJIlAaBlBeFfAuEp@yAzDcF`B_C`BqB~LuN|BgC`Ae@pGaCrBuAbLuIxCeCv@oAz@gCjCsItFeQN}BO_BOm@}BsDqBuCcF_HmA}A{A_A}BeAyBoAgAoAaAoAeAiBiDqLcA_DaDmIaA_CcFyImAoCCcBLgC\\mD`@gBtByFfK_Z|@_CtKuTdEcIjDsGnDsGnCiFjCgDvJuLbZg^dYq\\vRcU",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.977711,
                145.016899
            ]
        },
        {
            "avg_grade": 7.7,
            "climb_category": 3,
            "climb_category_desc": "2",
            "distance": 5178.8,
            "elev_difference": 401.1,
            "end_latlng": [
                -37.877989,
                145.327388
            ],
            "id": 374785,
            "name": "Devil's Elbow (full)",
            "points": "j`hfFqb|uZQmJIyBSiA]qAs@mBQo@U_BI[m@uAMg@OeASaA]u@e@i@]Uu@QmBQm@IoBm@u@Ge@B_B^q@Ac@_@GMMq@Fa@Tu@fAuAPc@bByFRu@\\oCJ]j@iAT[zCmBbCeBrCgCx@gAz@wBXc@b@g@Ng@CK]GuApAm@f@wAt@g@Rm@Ng@JwANcHLoANaEhAaAh@kAx@e@Ni@Ag@KYS][Yk@Kc@Ce@NkL?uEIwAQa@[Ug@UkBUo@?aATaBr@w@P{@BgAAc@Da@JS`@Cb@BlAI`@Yd@IRwArCo@l@cBn@_ANYNoAtAWRmAzAeBpCi@h@_@ViAj@uAf@y@`@_A^{@PYBiBd@mBPM?]Rc@D_@HwAb@o@@EURq@fAiBPQp@[d@Mb@IZU|@qAVe@xAuFVoAvBcJ",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.893333,
                145.311292
            ]
        },
        {
            "avg_grade": -6.4,
            "climb_category": 0,
            "climb_category_desc": "NC",
            "distance": 1028.4,
            "elev_difference": 70.0,
            "end_latlng": [
                -37.943633794784546,
                145.2742713689804
            ],
            "id": 816944,
            "name": "Comm Games - Descent",
            "points": "vxqfF{|suZt@Cl@Ql@Ij@Qz@g@h@K^]^e@bAq@`BoB`Aq@h@wBBsDaAuEQk@FAi@h@]u@g@Ji@e@Ec@CXIZ[g@Gm@GKAGM@e@NOYW^OYUCc@d@c@Fa@^e@X",
            "resource_state": 2,
            "starred": False,
            "start_latlng": [
                -37.94331729412079,
                145.26942193508148
            ]
        }
    ]
}