"""
Author:             Zi C
Date Created:       16-May-18
Date Modified:      16-May-18
Modified by:        Zi C
"""
import random
import unittest
from unittest import mock
import json
from services.strava_api_service import *
from dtos import leaderboard_dto


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, text, status_code):
            self.text = text
            self.status_code = status_code
    if args[1] == 'https://www.strava.com/api/v3/segments/explore':
        segment_id = 123
        distance = 1234
        name = "SomeSegment"
        return MockResponse("{{\"segments\": [{{\"id\": {}, \"distance\": {}, \"name\": \"{}\"}}]}}".format(segment_id, distance, name), 200)
    if args[1] == "https://www.strava.com/api/v3/segments/123/leaderboard":
        leaderboard_entries = list()
        leaderboard_entries.append(leaderboard_dto.StravaLeaderBoardEntry(athlete_name="John",
                                                                          elapsed_time=555,
                                                                          moving_time=555,
                                                                          start_date="2017-12-23T04:23:19Z",
                                                                          start_date_local="2017-12-23T04:23:19Z",
                                                                          rank=1))
        return MockResponse(json.dumps(SAMPLE_LEADERBOARD_DATA), 200)
    return MockResponse(None, 404)


class TestSegmentApiService(unittest.TestCase):
    def test_validate_lower_bound(self):
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(-91, 0, 0, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, 0, -91, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, -181, 0, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, 0, 0, -181))

    def test_validate_upper_bound(self):
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(91, 0, 0, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, 181, 0, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, 0, 91, 0))
        with self.assertRaises(AssertionError):
            self.assertRaises(validate_latitude_longitudes(0, 0, 0, 181))

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_segment_should_succeed(self, mock_get):
        o = call_get_for_segments(lat2=-37.5112737, long2=145.5125288, lat1=-38.4338593, long1=144.5937418, api_key="123")
        self.assertEqual(len(o), 1)
        self.assertEqual(o[0].segment_id, 123)
        self.assertEqual(o[0].distance, 1234)
        self.assertEqual(o[0].name, "SomeSegment")

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_segment_should_fail_when_lat2_is_out_of_bounds(self, mock_get):
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-99, long2=145.5125288, lat1=-38.4338593, long1=144.5937418,
                                      api_key="123")
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=99, long2=145.5125288, lat1=-38.4338593, long1=144.5937418,
                                      api_key="123")

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_segment_should_fail_when_long2_is_out_of_bounds(self, mock_get):
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37, long2=-99, lat1=-38.4338593, long1=144.5937418,
                                      api_key="123")
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37, long2=99, lat1=-38.4338593, long1=144.5937418,
                                      api_key="123")

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_segment_should_fail_when_lat1_is_out_of_bounds(self, mock_get):
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37.5112737, long2=145.5125288, lat1=-99, long1=144.5937418,
                                      api_key="123")
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37.5112737, long2=145.5125288, lat1=99, long1=144.5937418,
                                      api_key="123")

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_segment_should_fail_when_long1_is_out_of_bounds(self, mock_get):
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37.5112737, long2=145.5125288, lat1=-38.4338593, long1=-99,
                                      api_key="123")
        with self.assertRaises(AssertionError):
            o = call_get_for_segments(lat2=-37.5112737, long2=145.5125288, lat1=-38.4338593, long1=99,
                                      api_key="123")


class TestLeaderboardApiService(unittest.TestCase):
    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_leaderboard_should_succeed(self, mock_get):
        o = call_get_for_leaderboard(segment_id=123, api_key="123")
        self.assertEqual(len(o.entries), len(SAMPLE_LEADERBOARD_DATA["entries"]))
        leaderboard = o.entries[0]
        self.assertEqual(leaderboard.rank, SAMPLE_LEADERBOARD_DATA["entries"][0]["rank"])
        self.assertEqual(leaderboard.athlete_name, SAMPLE_LEADERBOARD_DATA["entries"][0]["athlete_name"])

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_leaderboard_should_succeed_when_given_male_gender(self, mock_get):
        o = call_get_for_leaderboard(segment_id=123, api_key="123", gender="M")
        self.assertEqual(len(o.entries), len(SAMPLE_LEADERBOARD_DATA["entries"]))
        leaderboard = o.entries[0]
        self.assertEqual(leaderboard.rank, SAMPLE_LEADERBOARD_DATA["entries"][0]["rank"])
        self.assertEqual(leaderboard.athlete_name, SAMPLE_LEADERBOARD_DATA["entries"][0]["athlete_name"])

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_leaderboard_should_succeed_when_given_female_gender(self, mock_get):
        o = call_get_for_leaderboard(segment_id=123, api_key="123", gender="F")
        self.assertEqual(len(o.entries), len(SAMPLE_LEADERBOARD_DATA["entries"]))
        leaderboard = o.entries[0]
        self.assertEqual(leaderboard.rank, SAMPLE_LEADERBOARD_DATA["entries"][0]["rank"])
        self.assertEqual(leaderboard.athlete_name, SAMPLE_LEADERBOARD_DATA["entries"][0]["athlete_name"])

    @mock.patch("services.strava_api_service.requests.request", side_effect=mocked_requests_get)
    def test_call_get_for_leaderboard_should_fail_when_given_incorrect_gender(self, mock_get):
        with self.assertRaises(AssertionError):
            o = call_get_for_leaderboard(segment_id=123, api_key="123", gender="T")
        with self.assertRaises(AssertionError):
            o = call_get_for_leaderboard(segment_id=123, api_key="123", gender="MF")


SAMPLE_LEADERBOARD_DATA = \
    {
        "effort_count": random.randint(0, 10000),
        "entry_count": 18743,
        "kom_type": "kom",
        "entries": [
            {
                "athlete_name": "Jimmy W.",
                "elapsed_time": 524,
                "moving_time": 524,
                "start_date": "2017-12-23T04:23:19Z",
                "start_date_local": "2017-12-23T15:23:19Z",
                "rank": 1
            }
        ]
    }
if __name__ == '__main__':
    unittest.main()
