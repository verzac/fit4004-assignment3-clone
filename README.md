# FIT 4004 ASSIGNMENT 3

DEVELOPERS: NIKHIL SHARMA, BENJAMIN TANONE, ZI CHOONG

STAFF: ROBYN MCNAMARA, STAPHEN MCNAMARA, ROBERT MERKEL

## Dependencies
* Python3
* pip

More information regarding additional dependencies can be obtained from requirements.txt.

## Running the program
```bash
pip install -r requirements.txt
python app.py
```

## Testing the program
```bash
# unittest
python -m unittest discover -s test
```

## Code Coverage Report
[Link to Report Page](https://nikhilsharma66.gitlab.io/FIT4004-Assignment3/index.html)

## Dev Notes

    What the main function (respect to the API, not application) should do:
    1. GET segments by location -> [segments]
    2. for each |segment| in [segments], GET leaderboard by segment
    3. for each leaderboard get fastest time
    4. populate fastest times into array
    5. select n slowest times from array

    <lat1> <long1> <lat2> <long2> (Mandatory) [Lat must be between -90 to 90, Long must be between -180 and 180]
    <access key> (Mandatory)
    -g <"M"/"F"> (If not specified, use both genders, do not filter)
    -n <number of segments to display> (If not specified, default to 5)
    -o <output filename> (If not specified, default to out.pdf)

    e.g StravaGraph 01 02 03 04 abc123apiKey

    Sections:
    API integration
    Graphing capability (And display)
    Write graph to file capability

Conventions:
- Any methods with a call to the API should have 'call' in it's method name: e.g. call_post_to_update_user_data()
- Test methods should be prefixed with 'test_' and have the overall structure of 'method_should_do_X_when_Y'